package app.com.storagemodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private StorageModule storageModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storageModule = new StorageModule(this);

        test();
    }

    public void test(){
        if(StorageTests.test(storageModule)){
            Log.d("MAIN_ACTIVITY", "WELL DONE");
        }else{
            Log.d("MAIN_ACTIVITY", "SOME ERRORS");
        }
    }
}
