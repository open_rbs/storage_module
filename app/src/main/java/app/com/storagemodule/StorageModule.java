package app.com.storagemodule;

import android.app.Activity;

import java.util.List;

/**
 * <p>Класс - модуль для работы с данными</p>
 * <p>С его помощью сохраняються и загружаються данные для приложения</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class StorageModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "STORAGE_MODULE";

    /**
     * <p>инстанс данного класса</p>
     */
    private static StorageModule instance;

    private Activity activity;
    /**
     * <p>Приватный конструктор для реализации синглтона</p>
     */
    public StorageModule(Activity _activity){
        activity = _activity;
    }

    /**
     * <p>Сохраняет значение по ключу в указанное хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     * @param value Значение
     */
    public boolean save(String storageName, String key, String value){
        return true;
    }

    /**
     * <p>Возвращает значение по ключу в указанном хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     * @return знычение либо пустую строку если нету такого поля в базе
     */
    public String load(String storageName, String key){
        return "";
    }

    /**
     * <p>Удаляет значение по ключу в указанное хранилище</p>
     * @param storageName Название хранилища
     * @param key Ключ по которому будет хранить значение
     */
    public boolean delete(String storageName, String key){
        return true;
    }

    /**
     * <p>Возвращает все значения в указанном хранилище</p>
     * @param storageName Название хранилища
     */
    public List<String> loadAll(String storageName){
        return null;
    }

    /**
     * <p>Удаляет все значения в указанном хранилище</p>
     * @param storageName Название хранилища
     */
    public boolean deleteAll(String storageName){
        return true;
    }

}
