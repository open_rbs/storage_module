package app.com.storagemodule;

import android.util.Base64;
import android.util.Log;

import java.util.List;


/**
 * Created by ruboss on 2/28/18.
 *
 * test methods reader
 *  load
 *  loadAll
 *
 * test methods writer
 *  save
 *  delete
 *  deleteAll

 */

public class StorageTests {
    private static final String LOG = "STORAGE_TEST";

    public static boolean test(StorageModule storageModule){
        return part1(storageModule) && part2(storageModule);
    }

    private static boolean part2(StorageModule storageModule){

        if(!storageModule.load("STORAGE", "key").equals("value2")){
            Log.d(LOG, "failed load 1");
            return false;
        }

        if(storageModule.load("STORAGE2", "key").equals("")){
            Log.d(LOG, "failed load 2");
            return false;
        }

        if(storageModule.load("STORAGE", "key2").equals("value")){
            Log.d(LOG, "failed load 2");
            return false;
        }

        if(storageModule.loadAll("STORAGE").size() != 2){
            Log.d(LOG, "failed loadAll 1");
            return false;
        }

        if(storageModule.loadAll("STORAGE3").size() != 0){
            Log.d(LOG, "failed loadAll 2");
            return false;
        }

        return true;
    }


    private static boolean part1(StorageModule storageModule){
        if(!storageModule.save("STORAGE", "key", "value")){
            Log.d(LOG, "failed save");
            return false;
        }

        if(!storageModule.save("STORAGE", "key", "value2")){
            Log.d(LOG, "failed save 2");
            return false;
        }

        if(!storageModule.save("STORAGE", "key2", "value")){
            Log.d(LOG, "failed save 3");
            return false;
        }


        if(!storageModule.save("STORAGE", "key3", "value")){
            Log.d(LOG, "failed save");
            return false;
        }

        if(!storageModule.save("STORAGE2", "key", "value")){
            Log.d(LOG, "failed save st2");
            return false;
        }

        if(!storageModule.delete("STORAGE2", "key")){
            Log.d(LOG, "failed delete st2");
            return false;
        }

        if(!storageModule.save("STORAGE3", "key", "value")){
            Log.d(LOG, "failed save st3");
            return false;
        }

        if(!storageModule.save("STORAGE3", "key2", "value")){
            Log.d(LOG, "failed save st3");
            return false;
        }

        if(!storageModule.save("STORAGE3", "key3", "value")){
            Log.d(LOG, "failed save st3");
            return false;
        }

        if(!storageModule.deleteAll("STORAGE3")){
            Log.d(LOG, "failed deleteAll st3");
            return false;
        }

        return true;
    }



}
